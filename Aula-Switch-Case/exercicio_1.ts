//Escreva um programa que pergunte ao usuário qual o seu nível de conhecimento em TypeScript e exiba uma mensagem de acordo com o nível escolhido:
namespace exercicio_1
{
    let nivel: string;
    nivel = "Avancado";

    switch(nivel)
    {
        case "Avancado":console.log("Uau, Parabéns! Seu nível é AVANÇADO!");
                        break;
        case "Intermediario":console.log("Uau! Seu nível é INTERMEDIÁRIO!");
                        break;
        case "Basico":console.log("Quase lá! Seu nível é BÁSICO!")
                        break;
        default: console.log("Não é possível reconhecer esse nível!")
    }
}
