namespace introFOR {
  for (let i = 0; i < 3; i++) {
    console.log("Esta é interação de número" + i);
  }
  //mostrar todos os numeros impares
  for (let i = 0; i < 100; i++) {
    if (i % 2 != 0) {
      console.log(`O número ${i} é par`);
    }
  }
  //tabuada do 2
  for (let i = 0; i <= 10; i++) {
    let resultado: number;
    resultado = 2 * i;
    console.log(`2 x ${i} = ${resultado}`);
  }
}
